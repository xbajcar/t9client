// @flow
import * as React from "react";
import Keypad from "./keypad";
import { getWords } from "../services/wordService";
import TextArea from "./textArea";
import style from "styled-components";

const T9Content = style.div`
  width: 50%;
  height: 100vh;
  max-width: 30%;
  margin: auto;
  font-size: 2em;
  @media only screen and (max-width: 1000px) {
    width: 90%;
    max-width: 90%;
  }
`;

const ErrorText = style.p`
  color: #bd2b20;
  font-size: 0.5em;
`;

const CustomCheckbox = style.input.attrs({ type: "checkbox" })`
  color: #bd2b20;
  font-size: 0.5em;
  span {
    color: #2b47c4;
    font-size: 0.5em;
  }
`;

const InputLabel = style.span`
  color: #2b47c4;
  font-size: 0.5em;
`;

type State = {
  text: string, //already written text
  selectedWord: string, //new word to be added to text
  words: string[], //all textonyms of textonym
  textonym: string, //t9 representation of selectedWord
  textonyms: string[], //t9 representation of text
  error: string, //error message
  useWords: boolean, //determines whether to use words api or generator api
};
//Encapsulates a T9 keyboard and a display for output
class T9 extends React.Component<{}, State> {
  state = {
    text: "",
    selectedWord: "",
    words: [],
    textonym: "",
    textonyms: [],
    error: "",
    useWords: false,
  };

  //returns all textonyms (words) to 'textonym'
  getTextonyms = async (textonym: string) => {
    try {
      const { data: words }: { data: string[] } = await getWords(
        textonym,
        this.state.useWords
      );
      this.setState({
        textonym,
        selectedWord: words[0],
        words,
        error: "",
      });
    } catch (err) {
      if (err.response) {
        console.log(err.response.data);
      } else {
        console.log(err);
        this.setState({ error: "Could not connect to server." });
      }
    }
  };

  //adds one 'number' to 'textonym' and calls getTextonyms
  handleDial = (number: number) => {
    const { textonym } = this.state;
    const newTextonym = textonym + number;
    this.getTextonyms(newTextonym);
  };

  //adds the 'selectedWord' and one empty space to 'text'
  handleSpace = () => {
    const { text, selectedWord, textonym, textonyms } = this.state;
    if (textonym) textonyms.push(textonym);
    const newText = `${text} ${selectedWord}`;
    this.setState({
      text: newText,
      textonym: "",
      textonyms,
      selectedWord: "",
      words: [],
    });
  };

  //sets the next textonym from 'words' as 'selectedWord'
  handleNext = () => {
    const { words, selectedWord } = this.state;
    if (!selectedWord || words.length === 0) return;
    const index: number = words.indexOf(selectedWord);
    const nextIndex: number = (index + 1) % words.length;
    this.setState({ selectedWord: words[nextIndex] });
  };

  //removes last character from output and calls getTextonyms
  handleDelete = () => {
    const { textonym } = this.state;
    const newTextonym: string = textonym
      ? this.changeTextonym(textonym)
      : this.changeText();
    if (newTextonym) this.getTextonyms(newTextonym);
  };

  //removes last number from 'textonym'
  changeTextonym = (textonym: string): string => {
    const newTextonym: string = textonym.slice(0, -1);
    if (newTextonym.length === 0) {
      this.setState({
        textonym: "",
        selectedWord: "",
        words: [],
        error: "",
      });
    }
    return newTextonym;
  };

  //sets the last word as 'textonym'
  changeText = () => {
    const { textonyms, text } = this.state;
    const newTextonym = textonyms.pop();
    const newText = text.split(" ").slice(0, -1).join(" ");
    this.setState({ textonyms, text: newText });
    if (newTextonym) this.getTextonyms(newTextonym);
    return newTextonym;
  };

  setUseWords = (e: SyntheticEvent<HTMLInputElement>) => {
    this.setState({ useWords: e.currentTarget.checked });
  };

  render() {
    const { text, selectedWord, error } = this.state;
    return (
      <T9Content>
        <TextArea text={text} word={selectedWord} />
        {error && <ErrorText>{error}</ErrorText>}
        <Keypad
          onDial={this.handleDial}
          onSpace={this.handleSpace}
          onNext={this.handleNext}
          onDelete={this.handleDelete}
        />
        <div>
          <InputLabel>Use words</InputLabel>
          <CustomCheckbox onChange={this.setUseWords} />
        </div>
      </T9Content>
    );
  }
}

export default T9;
