// @flow
import * as React from "react";
import styled from "styled-components";

const KeypadGrid = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  margin: auto;
  padding: 0px;
`;

const KeypadItem = styled.button`
  margin: 0.5em;
  text-align: center;
`;

const Key = styled(KeypadItem)`
  height: 8vh;
  font-size: 20px;
  border-radius: 12px;
  cursor: pointer;
  outline: none;
  color: #2b47c4;
  background-color: #fff;
  border: 1px solid #2b47c4;
  box-shadow: 0 4px #dcdcdc;

  &:active {
    box-shadow: 0 2px #c4c4c4;
    transform: translateY(0.1em);
  }

  p {
    line-height: 0px;
  }
`;

type Props = {
  onDial: (number) => void,
  onSpace: () => void,
  onNext: () => void,
  onDelete: () => void,
};

//renders a T9 keypad - 9 numbered buttons + Next, Space, and Delete
const Keypad = ({ onDial, onSpace, onNext, onDelete }: Props) => {
  const keypad = {
    "2": "a b c",
    "3": "d e f",
    "4": "g h i",
    "5": "j k l",
    "6": "m n o",
    "7": "p q r s",
    "8": "t u v",
    "9": "w x y z",
  };

  return (
    <KeypadGrid>
      <Key>1</Key>
      {Object.keys(keypad).map((key) => {
        return (
          <Key key={key} onClick={() => onDial(parseInt(key))}>
            <div>
              <p>{key}</p>
              <p>{keypad[key]}</p>
            </div>
          </Key>
        );
      })}
      <Key onClick={() => onNext()}>Next</Key>
      <Key onClick={() => onSpace()}>Space</Key>
      <Key onClick={() => onDelete()}>Delete</Key>
    </KeypadGrid>
  );
};

export default Keypad;
