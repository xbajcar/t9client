// @flow
import * as React from "react";
import styled from "styled-components";

const TextAreaBlock = styled.div`
  white-space: pre-wrap;
  height: 30%;
  overflow: scroll;
  padding: 5%;
  margin: 5% 0%;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.8);
`;

//represents a display of a t9 styled cell phone
class TextArea extends React.Component<{ text: string, word: string }> {
  textAreaEnd: ?HTMLDivElement; //dummy element to scroll to on overflow

  componentDidUpdate() {
    this.scrollToBottom();
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    if (this.textAreaEnd) {
      this.textAreaEnd.scrollIntoView(false);
    }
  };

  render() {
    const { text, word } = this.props;
    return (
      <TextAreaBlock>
        {text} <b>{word || "_"}</b>
        <div
          ref={(el) => {
            this.textAreaEnd = el;
          }}
        ></div>
      </TextAreaBlock>
    );
  }
}

export default TextArea;
