// @flow
import http from "./httpService";
import { word_api, generator_api } from "../configuration/config.json";

export function getWords(
  textonym: string,
  useWordsApi: boolean
): Promise<{ data: string[] }> {
  const url: string = useWordsApi ? word_api : generator_api;
  return http.get(url + textonym);
}
